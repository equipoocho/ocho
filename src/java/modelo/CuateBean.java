/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Cuate;
import controlador.CuateBuscaInterface;
import controlador.MapeaCuates;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author José Alberto R
 */
@ManagedBean(name="Cuate")
@SessionScoped
public class CuateBean {

    /**
     * Creates a new instance of CuateBean
     */
    
    private String user;
    private String password;
    private Cuate cuate;
    private Collection <Cuate> ListaCuates=null; 
    
    @ManagedProperty(value="#{buscaCuates}")
    private CuateBuscaInterface cuates;
//    private static CuateBuscaInterface cuates=new MapeaCuates();
    
    public CuateBean() {
    }

    public Collection<Cuate> getListaCuates() {
        ListaCuates=cuates.ListaCuates();
        return ListaCuates;
    }
    
    
    public Cuate getCuate(){
        return cuate;
    }

    public void setCuate(Cuate cuate) {
        this.cuate = cuate;
    }

    public CuateBuscaInterface getCuates() {
        return cuates;
    }

    public void setCuates(CuateBuscaInterface cuates) {
        this.cuates = cuates;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String pagoCuotas(){
        String pagina="";
        cuate= cuates.BuscaCuate(user, password);
        if(cuate==null){
            pagina="Intruso";
        }
        else
            if(cuate.getCuota()>300){
                pagina="Deudor";
            }
        else
            pagina="ListaCuates";
        return pagina;
    }
    
}
