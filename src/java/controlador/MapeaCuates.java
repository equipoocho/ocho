/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author José Alberto R
 */
@ManagedBean(name="buscaCuates", eager = true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface{

    private Map<String,Cuate> cuates;
    private Collection lista;
    
    public MapeaCuates() {
        lista = new ArrayList<>();
        cuates = new HashMap<>();
        addCuate(new Cuate("dano", "123456", "Daniel", "Olivares Palomino", 300.25, "fotoDano"));
        addCuate(new Cuate("beto", "123456", "José Alberto", "Ramírez Morales", 200.96, "fotoBeto"));
        addCuate(new Cuate("robert", "123456", "Roberto Daniel", "Sánchez Sánchez", 301.30, "fotoRobert"));
        addCuate(new Cuate("luist", "123456", "Luis", "Terrazas López", 500.01, "fotoLuist"));
        addCuate(new Cuate("caro", "123456", "Carolina", "Tizapantzi Sánchez", 900.95, "fotoCaro"));        
    }
    
    @Override
    public Cuate BuscaCuate(String login, String pasword) {
        Cuate cuate=null;
        if(login!=null){
            cuate= cuates.get(login.toLowerCase());
            if(cuate!=null){
            if(cuate.getPwd().equals(pasword)){
                return cuate;
            }
            else
                return null;
        }
        }
        return cuate;
    }

    private void addCuate(Cuate cuate) {
        cuates.put(cuate.getUser(), cuate);
    }

    public Map<String, Cuate> getCuates() {
        return cuates;
    }

    public void setCuates(Map<String, Cuate> cuates) {
        this.cuates = cuates;
    }

    public Collection getLista() {
        return lista;
    }

    public void setLista(Collection lista) {
        this.lista = lista;
    }
    
    
    @Override
        public Collection <Cuate> ListaCuates()
        {
            lista=cuates.values();
            return lista;
        }
    
}
