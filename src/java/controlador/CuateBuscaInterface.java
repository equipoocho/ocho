/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.Collection;

/**
 *
 * @author José Alberto R
 */
public interface CuateBuscaInterface {
    public Cuate BuscaCuate(String login,String pasword);
    public Collection<Cuate> ListaCuates();
}
